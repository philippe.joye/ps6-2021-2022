---
version: 2
type de projet: Projet de semestre
année scolaire: 2021/2022
titre: Optimisation de la consomation des véhicules
abréviation: OptoConso
filières:
  - Télécommunications
  - Informatique
mandants:
  - Climate Services SA
nombre d'étudiants: 1
mots-clés: [Positionnement, Climat, GPS, Mobile]
langue: [F,E,D]
confidentialité: non
réalisation: labo
suite: non
nombre d'étudiants: 1
---
## Description/Contexte
La lutte contre le réchauffement climatique est une priorité pour la presque totalité des gouvernements et le grand public est de plus en plus engagé pour une réduction des émissions de CO2. En même temps, la crise sanitaire a fait flamber les prix des carburants et certains pays mettent en place des mesures pour limiter l'impact sur les personnes les plus démunies. Dans les deux cas, une réduction rapide, sans frais, de la consommation des véhicules apporterait une partie de la solution. 

[Climate services SA](https://www.climate-services.ch/)

## Requirements

Développer une application mobile qui utilise les systèmes de navigation existant et ajoute une fonctionnalité qui permet de saisir l'heure d'arrivée souhaitée et qui retourne la vitesse instantanée optimale à respecter. L'application de doit pas être un système complet de navigation autonome, en concurrence avec les systèmes existants, mais représenter un ajout aux systèmes existants. 

## Objectifs

Le présent projet vise à développer une application mobile qui permet de limiter la consommation de carburant sur les déplacements long avec tout type de véhicules. Cette application doit également permettre d'augmenter l'autonomie des véhicules électriques et à augmenter la sécurité des automobilistes. 

Le concept est basé sur deux constats : 

1. à une vitesse entre 100 et 120 km/h une augmentation de la vitesse de 10 km/h augmente la consommation des véhicules de 10%. 
2.	Aujourd'hui, si vous souhaitez vous rendre à une destination, les systèmes de navigation indiquent le temps de parcours en fonction de la vitesse autorisée et ne vous permettent pas d'enter l'heure d'arrivée souhaitée.

La proposition est donc la suivante : Ajouter aux systèmes de navigations actuelles une fonctionnalité qui permet de saisir l'heure d'arrivée souhaitée. Le système de navigation recalcule alors la vitesse optimale de circulation pour arriver à destination à l'heure, mais en roulant à une vitesse qui minimise la consommation. La vitesse optimale à respecter est affichée en temps réel et actualisée en continu pour assurer une arrivée dans les temps.

L'idée est donc d'utiliser une éventuelle marge de temps sur un trajet pour limiter la vitesse et donc la consommation. De tels systèmes existent pour la navigation maritime, mais n'ont à notre connaissance jamais été appliqués au trafic routier. Avec une marge de 6 minutes sur un trajet d'une heure, le conducteur pourrait réduire de 10% ces émissions de CO2 et ces couts de carburant. De la même manière, l'autonomie d'un véhicule électrique aura augmenté de 10%. 
