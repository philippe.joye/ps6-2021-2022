---
version: 2
type de projet: Projet de semestre
année scolaire: 2021/2022
titre: Aligneur de bouchons
abréviation: PosBouchon2
filières:
  - Télécommunications
mandants:
  - Polytype SA
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Jacques Supcik
mots-clés: [MCA, Système embarqué, Image Processing, Commande]
langue: [F,E,D]
confidentialité: non
réalisation: entreprise
suite: oui
nombre d'étudiants: 1
proposé par étudiant: Nicolas Wirth

---
## Description/Context
L'entreprise Polytype développe, construit et installe des machines capables d'imprimer des motifs de couleur sur des tubes de plastique de taille et de formes variées. Dans le cadre du programme MCA (Motion Control Academie), l'entreprise Polytype demande de développer une machine complète capable d'aligner et de positionner des bouchons de manière à pouvoir ensuite les sertir correctement sur les tubes.

Il s'agit de développer, en équipe avec un étudiant GM et un étudiant GE qui seront en charge des aspects mécanique resp. électronique du système, la partie informatique (IT) du projet " Aligneur de bouchon ".

Le système sera capable de détecter sans contact (analyse optique, image ou autre...) l'orientation d'un bouchon flip-top (de tube ou de bouteille ) alimenté d'un tapis roulant où il est posé debout, ceci sur la base de sa forme légèrement non circulaire ou présentant une particularité (p. exemple clapet) qui en définit l'orientation. Sur la base de cette information, il s'agira alors de réorienter le bouchon sur le tapis roulant. Le projet de semestre 5 a permis d'explorer quelques solutions et d'en évaluer la faisabilité. Ce projet aura pour but de finaliser les choix techniques, d'en définir l'architecture détaillée, de commander le matériel et de réaliser la mise en oeuvre physique.

Sur la base des travaux de semestre précédents qui ont étudié les faisabilités techniques des différentes solutions, l'étudiant concrétisera la partie numérique (mesure, sauvegarde et traitement) du système de détection et de positionnement du bouchon.

Le projet de semestre 6 est arrivé à la conclusion que deux pistes pouvaient être explorées en parallèle : 

1. la détection par traitement et analyse d’image au moyen d’une caméra linéaire (line scan) et 

2. la détection par l’analyse du profil unidimensionnel du pourtour du bouchon au moyen d’un capteur de barrière lumineuse. 

Le travail de Bachelor consiste en la réalisation, le test et la validation d’un prototype du système complet intégrant ces deux options.

## Contraintes

* Contraintes organisationnelles induites par la conduite du programme MCA, incluant la définition interdisciplinaire, la gestion globale du projet et la coordination avec le mandant.
* Utilisation et mise en oeuvre des différents processus de développement recommandés par Polytype SA
* Utilisation d'un système de traitement minimisant les ressources (calcul, mémoire, énergie, etc.) et compatible avec les intentions du mandant.

## Objectifs/Tâches

* Finalisation des choix techniques et architecturaux du système en collaboration étroite avec les étudiants des autres filières. 
* Définition fonctionnelle (spécification) du système de traitement numérique pour les deux solutions proposées ci-dessus.
* Commande, validation et mise en oeuvre des composants matériels et logiciels du système.
* Définition détaillée des structures logicielles et matérielles des composants à développer (conception).
* Développement et validation complète de la solution avec l'aide et la supervision du mandant.
* Contribution propre à l’étudiant et à la partie ISC, clairement identifiable, au rapport technique final livrés, ainsi qu’à la partie commune du rapport final
