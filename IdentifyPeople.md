---
version: 2
type de projet: Projet de semestre
année scolaire: 2021/2022
titre: Reconnaissance de personnes pour l'hôpital de Loukla
abréviation: Identify people for health
filières:
  - Télécommunications
  - Informatique
mandants:
  - Hôpital Lukla (Népal)
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Houda Chabbi
mots-clés: [Traitement d'image, médical, reconnaissance de patient]
langue: [F,E,D]
confidentialité: non
réalisation: labo
suite: non
nombre d'étudiants: 1
---
## Description/Contexte
L'hôpital de Loukla au Népal est un hôpital créé il y a 16 ans par la fondation Nicole Niquille, du nom de la première femme guide de montagne suisse. Il est destiné à une population que le relief et le manque de voies de communication isolent. Les communautés de la Vallée des Sherpas ne peuvent donc bénéficier ni de soins médicaux, ni de médicaments et encore moins de conseils en matière de santé.

Les soins sont prescrits aux populations locales en fonction des demandes, des visites et des excursions dans les vallées reculées. La principale difficulté rencontrée par les équipes médicales consiste à identifier et authentifier des patient.e.s afin de pouvoir assurer un suivi médical approprié. En effet, la population locale ne possède, en grande majorité pas de carte d'identité, ont des noms à consonance identique et ne connaissent pas leur date de naissance, qui d'ailleurs provient souvent de calendriers différents.

Le projet consiste à explorer les voies d'identification simple par prise d'image à l'aide d'un smartphone usuel et d'en définir des caractéristiques significatives qui permettraient d'authentifier un.e patient.e lors de la visite suivante. Cette identification sera ensuite reliée à une base de données de patients contenant les diverses pathologies et diagnostiques de santé.

[Hôpital de Lukla](http://www.hopital-lukla.ch/)

## Contraintes

* Tenir compte, dès le début du projet, des contraintes de consommation d'énergie

## Objectives/Tasks

* Recherche de solutions similaires utilisées dans un contexte hospitalier identique au Népal
* Recherche et évaluation de librairies logicielles de reconnaissance faciale permettant une identification et une authentification de personne
* Spécification, conception et intégration d'une solution dans une base mobile (smartphone ou tablette) permettant de tester et de valider les performances d'identification et d'authentification sur des bases d'une première série d'images faciales
